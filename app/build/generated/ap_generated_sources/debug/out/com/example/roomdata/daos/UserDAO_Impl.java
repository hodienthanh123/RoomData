package com.example.roomdata.daos;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.example.roomdata.entitys.User;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class UserDAO_Impl implements UserDAO {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<User> __insertionAdapterOfUser;

  private final EntityDeletionOrUpdateAdapter<User> __deletionAdapterOfUser;

  private final EntityDeletionOrUpdateAdapter<User> __updateAdapterOfUser;

  private final SharedSQLiteStatement __preparedStmtOfDeleAll;

  public UserDAO_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfUser = new EntityInsertionAdapter<User>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `user` (`id`,`_name`,`_address`,`_year`) VALUES (nullif(?, 0),?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, User value) {
        stmt.bindLong(1, value.getId());
        if (value.get_name() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.get_name());
        }
        if (value.get_address() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.get_address());
        }
        if (value.get_year() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.get_year());
        }
      }
    };
    this.__deletionAdapterOfUser = new EntityDeletionOrUpdateAdapter<User>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `user` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, User value) {
        stmt.bindLong(1, value.getId());
      }
    };
    this.__updateAdapterOfUser = new EntityDeletionOrUpdateAdapter<User>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `user` SET `id` = ?,`_name` = ?,`_address` = ?,`_year` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, User value) {
        stmt.bindLong(1, value.getId());
        if (value.get_name() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.get_name());
        }
        if (value.get_address() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.get_address());
        }
        if (value.get_year() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.get_year());
        }
        stmt.bindLong(5, value.getId());
      }
    };
    this.__preparedStmtOfDeleAll = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM user";
        return _query;
      }
    };
  }

  @Override
  public void insertUser(final User u) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfUser.insert(u);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleUser(final User u) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfUser.handle(u);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateU(final User u) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __updateAdapterOfUser.handle(u);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleAll() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleAll.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleAll.release(_stmt);
    }
  }

  @Override
  public List<User> getListUser() {
    final String _sql = "SELECT * FROM user";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "_name");
      final int _cursorIndexOfAddress = CursorUtil.getColumnIndexOrThrow(_cursor, "_address");
      final int _cursorIndexOfYear = CursorUtil.getColumnIndexOrThrow(_cursor, "_year");
      final List<User> _result = new ArrayList<User>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final User _item;
        final String _tmp_name;
        if (_cursor.isNull(_cursorIndexOfName)) {
          _tmp_name = null;
        } else {
          _tmp_name = _cursor.getString(_cursorIndexOfName);
        }
        final String _tmp_address;
        if (_cursor.isNull(_cursorIndexOfAddress)) {
          _tmp_address = null;
        } else {
          _tmp_address = _cursor.getString(_cursorIndexOfAddress);
        }
        final String _tmp_year;
        if (_cursor.isNull(_cursorIndexOfYear)) {
          _tmp_year = null;
        } else {
          _tmp_year = _cursor.getString(_cursorIndexOfYear);
        }
        _item = new User(_tmp_name,_tmp_address,_tmp_year);
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _item.setId(_tmpId);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<User> checkListUser(final String name) {
    final String _sql = "SELECT * FROM user WHERE user._name= ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (name == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, name);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "_name");
      final int _cursorIndexOfAddress = CursorUtil.getColumnIndexOrThrow(_cursor, "_address");
      final int _cursorIndexOfYear = CursorUtil.getColumnIndexOrThrow(_cursor, "_year");
      final List<User> _result = new ArrayList<User>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final User _item;
        final String _tmp_name;
        if (_cursor.isNull(_cursorIndexOfName)) {
          _tmp_name = null;
        } else {
          _tmp_name = _cursor.getString(_cursorIndexOfName);
        }
        final String _tmp_address;
        if (_cursor.isNull(_cursorIndexOfAddress)) {
          _tmp_address = null;
        } else {
          _tmp_address = _cursor.getString(_cursorIndexOfAddress);
        }
        final String _tmp_year;
        if (_cursor.isNull(_cursorIndexOfYear)) {
          _tmp_year = null;
        } else {
          _tmp_year = _cursor.getString(_cursorIndexOfYear);
        }
        _item = new User(_tmp_name,_tmp_address,_tmp_year);
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _item.setId(_tmpId);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<User> slUser(final String s) {
    final String _sql = "SELECT * FROM user WHERE user._name LIKE '%' || ? ||  '%'";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (s == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, s);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "_name");
      final int _cursorIndexOfAddress = CursorUtil.getColumnIndexOrThrow(_cursor, "_address");
      final int _cursorIndexOfYear = CursorUtil.getColumnIndexOrThrow(_cursor, "_year");
      final List<User> _result = new ArrayList<User>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final User _item;
        final String _tmp_name;
        if (_cursor.isNull(_cursorIndexOfName)) {
          _tmp_name = null;
        } else {
          _tmp_name = _cursor.getString(_cursorIndexOfName);
        }
        final String _tmp_address;
        if (_cursor.isNull(_cursorIndexOfAddress)) {
          _tmp_address = null;
        } else {
          _tmp_address = _cursor.getString(_cursorIndexOfAddress);
        }
        final String _tmp_year;
        if (_cursor.isNull(_cursorIndexOfYear)) {
          _tmp_year = null;
        } else {
          _tmp_year = _cursor.getString(_cursorIndexOfYear);
        }
        _item = new User(_tmp_name,_tmp_address,_tmp_year);
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _item.setId(_tmpId);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
