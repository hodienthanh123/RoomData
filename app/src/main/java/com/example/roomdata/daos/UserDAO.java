package com.example.roomdata.daos;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.roomdata.entitys.User;

import java.util.List;

@Dao
public interface  UserDAO {
    @Insert
    public void insertUser(User u);

    @Query("SELECT * FROM user")
    public List<User> getListUser();

    @Query("SELECT * FROM user WHERE user._name= :name")
    public List<User> checkListUser(String name);

    @Update
    public void updateU(User u);

    @Delete
    public void deleUser(User u);

    @Query("DELETE FROM user")
    public void deleAll();

    @Query("SELECT * FROM user WHERE user._name LIKE '%' || :s ||  '%'")
    public List<User> slUser(String s);
}
