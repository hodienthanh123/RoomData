package com.example.roomdata.adappers;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.roomdata.R;
import com.example.roomdata.daos.ItemClick;
import com.example.roomdata.entitys.User;

import java.util.List;

public class UserAdapper extends RecyclerView.Adapter<UserAdapper.UserAdapperViewHold> {

    private List<User> lu;
    private ItemClick itemClick;

    public UserAdapper(ItemClick itemClick) {
        this.itemClick = itemClick;
    }

    public void setData(List<User> lu) {
        this.lu = lu;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public UserAdapperViewHold onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        v= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user,parent,false);
        return new UserAdapperViewHold(v);
    }

    @Override
    public void onBindViewHolder(@NonNull UserAdapperViewHold holder, int position) {
        User u=lu.get(position);
        if(u==null)
            Log.d("lỗi khởi tạo---> ","fail");
        holder.txtName.setText(u.get_name());
        holder.txtaddress.setText(u.get_address());
        holder.tvY.setText(u.get_year()+"");
        Log.d("dong  ---> "," ->>>>> "+u.get_name() +" | "+u.get_address());
        holder.btnUpdateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClick.UpUser(u);
            }
        });
        holder.btnDeleUser.setOnClickListener(view ->{
            Toast.makeText(view.getContext(), "xóa user", Toast.LENGTH_LONG).show();
                itemClick.Deleuser(u);
        });
    }

    @Override
    public int getItemCount() {
        if(lu!=null)
            return lu.size();
        return 0;
    }

    public class UserAdapperViewHold extends RecyclerView.ViewHolder{
        private TextView txtName, txtaddress, tvY;
        private Button btnUpdateUser, btnDeleUser;
        public UserAdapperViewHold(@NonNull View itemView) {
            super(itemView);
            txtName=itemView.findViewById(R.id.tv_name);
            txtaddress=itemView.findViewById(R.id.tv_address);
            btnUpdateUser=itemView.findViewById(R.id.btnUpdate);
            btnDeleUser=itemView.findViewById(R.id.btnDlete);
            tvY=itemView.findViewById(R.id.tv_year);
        }
    }
}
