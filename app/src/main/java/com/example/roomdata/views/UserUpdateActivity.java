package com.example.roomdata.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.roomdata.R;
import com.example.roomdata.dbs.UserDatabase;
import com.example.roomdata.entitys.User;

public class UserUpdateActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private EditText edtName, edtAddress;
    private Button btnUUser;
    private User u;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_update);
        InitUI();
        u= (User) getIntent().getExtras().get("obj");
        if(u!=null){
            edtName.setText(u.get_name().trim());
            edtAddress.setText(u.get_address().trim());
        }
        btnUUser.setOnClickListener(view ->{
            updateUser(u);
        });
    }

    private void updateUser(User u) {
        String getName = edtName.getText().toString().trim();
        String getAddress = edtAddress.getText().toString().trim();
        if(TextUtils.isEmpty(getName) || TextUtils.isEmpty(getAddress))
            Toast.makeText(this, "Chưa nhập đầy đủ dữ liệu", Toast.LENGTH_LONG).show();
        else{
            u.set_name(getName);
            u.set_address(getAddress);
            UserDatabase.getInstance(this).userDAO().updateU(u);
            Toast.makeText(this, "Cập nhật thành công", Toast.LENGTH_LONG).show();
            Intent in=new Intent();
            setResult(UserUpdateActivity.RESULT_OK, in);
            finish();
        }

    }

    public void InitUI(){
        edtName=findViewById(R.id.txtUName);
        edtAddress=findViewById(R.id.txtUAddress);
        btnUUser=findViewById(R.id.btnUUser);
    }
}