package com.example.roomdata.views;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.roomdata.R;
import com.example.roomdata.adappers.UserAdapper;
import com.example.roomdata.daos.ItemClick;
import com.example.roomdata.dbs.UserDatabase;
import com.example.roomdata.entitys.User;
import com.example.roomdata.views.UserUpdateActivity;

import java.util.ArrayList;
import java.util.List;

public class UserActivity extends Activity {

    private static final int MY_REQUEST_CODE = 10;
    private LinearLayoutManager linearLayoutManager;

    private UserAdapper userAdapper;
    private List<User> ml;


    private RecyclerView recyclerView;
    private EditText edtName, edtAddress, txtSearchName, txtY;
    private TextView txtDeleAlll;
    private Button btnAdd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        InitUI();
        userAdapper =new UserAdapper(new ItemClick() {
            @Override
            public void UpUser(User u) {
                clickUpdateUser(u);
            }
            @Override
            public void Deleuser(User u) {
                deleUser(u);
            }
        });
        ml=new ArrayList<>();

        userAdapper.setData(ml);

        linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setAdapter(userAdapper);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btnAdd.setOnClickListener(UserView ->{
            AddUser();
            Log.d("btn --->>> ","Add User");
        });

        txtDeleAlll.setOnClickListener(UserView -> {
            deleAll();
            Log.d("btn --->>> ","delete User");
        });
        txtSearchName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH){
                    handelSearchUser();
                }
                return false;
            }
        });
        loadDB();
    }

    private void handelSearchUser() {
        String getName =txtSearchName.getText().toString().trim();
        ml.clear();
        ml=new ArrayList<>();
        ml = UserDatabase.getInstance(this).userDAO().slUser(getName);
        userAdapper.setData(ml);
        Toast.makeText(this, "data search ->>>> "+ml.size(), Toast.LENGTH_LONG).show();
        //loadDB();
        //deleAll();
    }

    private void deleAll() {
        if(ml==null)
            Toast.makeText(UserActivity.this, "Data Available ", Toast.LENGTH_LONG).show();
        else{
            new AlertDialog.Builder(this)
                    .setTitle("Config delete All Data")
                    .setMessage("Are you sure")
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            UserDatabase.getInstance(UserActivity.this).userDAO().deleAll();
                            Toast.makeText(UserActivity.this, "DataAll sussces ", Toast.LENGTH_LONG).show();
                            loadDB();
                        }
                    })
                    .setNegativeButton("NO", null)
                    .show();
        }
    }

    private void deleUser(User u) {
        new AlertDialog.Builder(this)
                .setTitle("Config delete User")
                .setMessage("Are you sure")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //delete user
                        UserDatabase.getInstance(UserActivity.this).userDAO().deleUser(u);
                        Toast.makeText(UserActivity.this, "Delete User susscesful", Toast.LENGTH_LONG).show();
                        loadDB();
                    }
                })
                .setNegativeButton("NO", null)
                .show();
    }

    private void clickUpdateUser(User u) {
        Toast.makeText(this, "sữa", Toast.LENGTH_LONG).show();
        Intent intent =new Intent(UserActivity.this, UserUpdateActivity.class);
        Bundle bundle =new Bundle();
        bundle.putSerializable("obj", u);
        intent.putExtras(bundle);
        this.startActivityForResult(intent,MY_REQUEST_CODE);

    }

    public void InitUI(){
        edtName=findViewById(R.id.txtName);
        edtAddress=findViewById(R.id.txtAddress);
        btnAdd=findViewById(R.id.btnAddUser);
        recyclerView=findViewById(R.id.rcv_user);
        txtSearchName=findViewById(R.id.txtNameSearch);
        txtDeleAlll=findViewById(R.id.txtDeleAll);
        txtY=findViewById(R.id.txtYear);
    }
    public void AddUser(){
        String getName = edtName.getText().toString().trim();
        String getAddress = edtAddress.getText().toString().trim();
        String getyear=txtY.getText().toString().trim();
        if(TextUtils.isEmpty(getName) || TextUtils.isEmpty(getAddress))
            return;
        User u =new User(getName, getAddress, getyear);
        if(checkUser(u)){
            Toast.makeText(this, "đã có rồi status->>>>"+checkUser(u), Toast.LENGTH_LONG).show();
            return;
        }
        UserDatabase.getInstance(this).userDAO().insertUser(u);
        Toast.makeText(this, "đã thêm thành công ->>> "+ml.size(), Toast.LENGTH_LONG).show();
        edtName.setText("");
        edtAddress.setText("");


        hideSoftKeyborad();
        loadDB();

    }
    private void loadDB(){
        ml=UserDatabase.getInstance(this).userDAO().getListUser();
        for(User u: ml){
            Log.d("dữ liệu ->>>>> "," "+ u.get_name()+"  ->>> "+u.getId());
        }
        userAdapper.setData(ml);
    }
    public boolean checkUser(User u){
        List<User> l=UserDatabase.getInstance(this).userDAO().checkListUser(u.get_name());
        //Toast.makeText(this, l.isEmpty()+ "kiểm tra list ->>> "+ml.size(), Toast.LENGTH_LONG).show();
        return l!=null && !l.isEmpty();
    }
    public void hideSoftKeyborad(){
        Log.d("Ẩn bàn phím ","----->>> ");
//        try {
//
//        }
//        catch (e){
//
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== MY_REQUEST_CODE && resultCode == UserActivity.RESULT_OK){
            loadDB();
            //Log.d("dữ liệu sang ","----->>> " + data.getData().toString());
        }
        Log.d("1 ->>>>>", ""+requestCode);
        Log.d("2 ->>>>>", ""+resultCode +" | "+UserActivity.RESULT_OK);
    }
}